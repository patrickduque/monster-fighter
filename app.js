new Vue({
	el: '#app',
	data: {
		playerHealth: 100,
		monsterHealth: 100,
		gameIsRunning: false,
		userAttackLogs: [],
	},
	methods: {
		playerDamage(dmg) {
			return Math.floor(Math.random() * dmg);
		}, 
		monsterDamage() {
			return Math.floor(Math.random() * 10);
		},
		attack() {
			if (this.playerHealth > 0 && this.monsterHealth > 0) {
				let pd = this.playerDamage(6)
				let md = this.monsterDamage()
				this.playerHealth -= md;
				this.monsterHealth -= pd;
				this.userAttackLogs.push(`You attacked the monster with ${pd} damage`);
				this.userAttackLogs.push(`Monster attacked you with ${md} damage`);
			} else if (this.playerHealth <= 0) {
				alert('Monster defeated You');
				this.gameIsRunning = false;
			} else if (this.monsterHealth <= 0) {
				alert('You defeated the monster')
				this.gameIsRunning = false;
			}
		},
		specialAttack() {
			if (this.playerHealth > 0 && this.monsterHealth > 0) {
				let pd = this.playerDamage(12)
				let md = this.monsterDamage()
				this.playerHealth -= md;
				this.monsterHealth -= pd;
				this.userAttackLogs.push(`You attacked the monster with ${pd} damage`);
				this.userAttackLogs.push(`Monster attacked you with ${md} damage`);
			} else if (this.playerHealth <= 0) {
				alert('Monster defeated You');
				this.gameIsRunning = false;
			} else if (this.monsterHealth <= 0) {
				alert('You defeated the monster')
				this.gameIsRunning = false;
			}
		},
		heal() {
			if (this.playerHealth === 100) {
				alert('You have full health');
			}
			else {
				let pd = this.playerDamage(12)
				let md = this.monsterDamage()
				this.playerHealth += pd;
				this.playerHealth -= md;
				this.userAttackLogs.push(`You healed ${pd} points`);
				this.userAttackLogs.push(`Monster attacked you with ${md} damage`);
			}	
		},
		reset() {
			this.gameIsRunning = !this.gameIsRunning;
			this.playerHealth = 100;
			this.monsterHealth = 100;
			this.userAttackLogs = [];
		}
	},
})